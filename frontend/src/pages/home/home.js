import React from 'react';
import './home.scss';
import Header from '../../components/header/header';
import Footer from '../../components/footer/footer';
import {Switch, Route} from 'react-router-dom';
import AdminTicketStatuses from '../../components/admin_ticket/admin_ticketstatuses';
import AdminTicketTypes from '../../components/admin_ticket/admin_tickettypes';
import AdminTickets from '../../components/admin_ticket/admin_tickets';
import AdminAddUser from '../../components/admin_adduser/admin_adduser';
import AdminGetUsers from '../../components/admin_getusers/admin_getusers';
import UserTickets from '../../components/user_ticket/user_tickets';
import UserFilters from '../../components/user_ticket/user_filters';
import { getIsAdminFromStorage } from '../../helpers/is-admin';
import PrivateRoute from '../../helpers/private-route';

class Home extends React.Component
{
  constructor(props)
  {
      super(props);
      this.state = {
          isAdmin: getIsAdminFromStorage()
      };
  }

  render()
  {
    return (<div className="Home">
      <Header isAdmin={this.state.isAdmin} history={this.props.history}/>
      <Switch>
        {/* Admin pages */}
        <PrivateRoute path="/home" exact component={AdminTickets} />
        <PrivateRoute path="/home/admin_ticketstatuses" component={AdminTicketStatuses} />
        <PrivateRoute path="/home/admin_tickettypes" component={AdminTicketTypes} />
        <PrivateRoute path="/home/admin_tickets" component={AdminTickets} />
        <PrivateRoute path="/home/admin_adduser" component={AdminAddUser} />
        <PrivateRoute path="/home/admin_getusers" component={AdminGetUsers} />
        {/* User pages */}
        <Route path="/home/user_tickets" component={UserTickets}/>
        <Route path="/home/user_filters" component={UserFilters}/>
      </Switch>
      <Footer/>
    </div>);
  }
}

export default Home;
