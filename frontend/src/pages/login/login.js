import React from 'react';
import './login.scss';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Cookies from 'universal-cookie';
import Footer from '../../components/footer/footer';
import { getIsAdminFromStorage } from '../../helpers/is-admin';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const cookies = new Cookies();

class Login extends React.Component
{
  constructor(props)
  {
    super(props);
    this.state =
    {
      username: '',
      password: '',
      checkbox: false
    };
    this.loginFormChange = this.loginFormChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount()
  {
    this.userInput.focus();
    if(getIsAdminFromStorage() === 'true')
    {
      this.props.history.push('/home/admin_tickets');
    } else
    {
      console.log("Backend URL from config: " + process.env.REACT_APP_BACKEND_SERVER);

      const request = new Request(`${process.env.REACT_APP_BACKEND_SERVER}/login`, {credentials: 'include'});

      fetch(request)
      .then(response => {
        const status = response.status;
        if (status === 200) {
          response.json()
          .then(userDataFromServer => {
            if (userDataFromServer.cookie)
            {
              cookies.set('login', userDataFromServer.cookie, {path: '/'});
            }
            sessionStorage.setItem('userId', userDataFromServer['_id']);
            sessionStorage.setItem('userName', userDataFromServer['name']);
            sessionStorage.setItem('isAdmin', userDataFromServer['isAdmin']);
            if (userDataFromServer['isAdmin'])
            {
              this.props.history.push('/home');
            } else
            {
              this.props.history.push('/home');
            }
          });
        } else if (status !== 400)
        {
          response.json()
          .then(serverError =>
          {
            alert(response.status + '\n' + serverError.message);
          });
        }
      });
    }

  }

  loginFormChange(event)
  {
    this.setState(
    {
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event)
  {
    event.preventDefault();

    const loginData =
    {
      'user': this.state.username,
      'pass': this.state.password,
      'remember-me': this.checkBoxInput.checked
    };

    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const options =
    {
      method: 'POST',
      headers,
      body: JSON.stringify(loginData)
    }

    const request = new Request(`${process.env.REACT_APP_BACKEND_SERVER}/login`, options);

    fetch(request).then(response =>
    {
      const status = response.status;
      if (status === 200) {
        this.loginForm.reset();
        response.json()
          .then(userDataFromServer =>
          {
            if (userDataFromServer.cookie)
            {
              cookies.set('login', userDataFromServer.cookie, {path: '/'});
            }
            sessionStorage.setItem('userId', userDataFromServer['_id']);
            sessionStorage.setItem('userName', userDataFromServer['name']);
            sessionStorage.setItem('isAdmin', userDataFromServer['isAdmin']);
            this.props.history.push('/home');
          });
      } else
      {
        this.loginForm.reset();
        response.json()
          .then(serverError =>
          {
            alert(response.status + '\n' + serverError.message);
          });
      }
    });
  }

  render()
  {
    return (
      <div align="center">
        <div className="Login">
          <Card style={{width: '90%'}} align="left" className={`mx-auto position-relative p-4 bg-light`}>
            <Card.Body className="card-body">
              <h3>Üdv újra itt!</h3>
              <h6>Kérlek, lépj be ha már van fiókod!</h6>
              <hr/><br/>
              <Form ref={(form) => {this.loginForm = form;}} validated={this.validated} onSubmit={this.handleSubmit}>
                <Form.Group as={Row} controlId="formBasicEmail">
                  <Form.Label column sm="2">Felhasználó</Form.Label>
                  <Col sm="10">
                    <Form.Control required ref={(input) => {this.userInput = input;}} name="username" type="text" onChange={this.loginFormChange}/>
                  </Col>
                </Form.Group>
                <br/>
                <Form.Group as={Row} controlId="formBasicPassword">
                  <Form.Label column sm="2">Jelszó</Form.Label>
                  <Col sm="10">
                    <Form.Control required name="password" type="password" onChange={this.loginFormChange}/>
                  </Col>
                </Form.Group>
                <br/>
                <Form.Group controlId="formBasicCheckbox">
                  <Form.Check ref={(check) => {this.checkBoxInput = check;}} name="checkbox" type="checkbox" label="Emlékezz rám"/>
                </Form.Group>
                <hr className="mt-4"/>
                <br/>
                <Button variant="primary" type="submit">
                  Belépés
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default Login;
