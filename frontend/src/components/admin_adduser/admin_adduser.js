import React from 'react';
import './admin_adduser.scss';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class AdminAddUser extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state =
        {
            name: '',
            email: '',
            username: '',
            password: '',
            checkbox: false,
        };
        this.registerFormChange = this.registerFormChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.clearRegisterForm = this.clearRegisterForm.bind(this);
    }

    componentDidMount()
    {
        this.nameInput.focus();
    }

    registerFormChange(event)
    {
        this.setState(
        {
            [event.target.name]: event.target.value
        });
    }

    clearRegisterForm()
    {
        this.registerForm.reset();
    }

    handleSubmit(event)
    {
        event.preventDefault();

        const registerData =
        {
            name: this.state.name,
            email: this.state.email,
            user: this.state.username,
            pass: this.state.password,
            isAdmin: this.isAdminCheckBoxInput.checked
        };

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        const options =
        {
            method: 'POST',
            headers,
            body: JSON.stringify(registerData)
        };

        const request = new Request(`${process.env.REACT_APP_BACKEND_SERVER}/signup`, options);

        fetch(request).then(response =>
        {
            const status = response.status;
            if (status === 201)
            {
                this.registerForm.reset();
                alert('Sikeresen felvettél egy felhasználót!');
            }
            else
            {
                response.json().then(serverError =>
                {
                    alert(response.status + '\n' + serverError.message);
                });
            }
        })
    }

  render() {
    return (
      <div className="AdminAddUser lg my-5 mx-auto">
        <br/><br/>
        <Card className="p-4 bg-light text-left">
          <Card.Body className="card-body">
            <h3>Fiók nyitása</h3>
            <h6>Töltsd ki az adatokat és nyomj a tovább gombra.</h6>
            <hr/><br/>
            <Form ref={(form) => {
                this.registerForm = form;
              }} onSubmit={this.handleSubmit}>
              <Form.Group as={Row} controlId="name">
                <Form.Label column sm="2">Név</Form.Label>
                <Col sm="10">
                  <Form.Control ref={(name) => {
                      this.nameInput = name;
                    }} name="name" type="text" required onChange={this.registerFormChange}/>
                </Col>
              </Form.Group>
              <br/>
              <Form.Group as={Row} controlId="email">
                <Form.Label column sm="2">Email</Form.Label>
                <Col sm="10">
                  <Form.Control name="email" type="email" required onChange={this.registerFormChange}/>
                </Col>
              </Form.Group>
              <br/>
              <Form.Group as={Row} controlId="username">
                <Form.Label column sm="2">Felhasználó</Form.Label>
                <Col sm="10">
                  <Form.Control name="username" type="text" required onChange={this.registerFormChange}/>
                </Col>
              </Form.Group>
              <br/>
              <Form.Group as={Row} controlId="password">
                <Form.Label column sm="2">Jelszó</Form.Label>
                <Col sm="10">
                  <Form.Control name="password" type="text" required onChange={this.registerFormChange}/>
                </Col>
              </Form.Group>
              <br/>
              <Row className="d-flex justify-content-sm-between">
                <Form.Group controlId="isAdminCheckBox" className="ml-3">
                  <Form.Check ref={(check) => {
                    this.isAdminCheckBoxInput = check;
                  }} name="checkbox" type="checkbox" label="Admin"/>
                </Form.Group>
                <hr className="mt-4"/>
                <div className="buttons">
                  <br/>
                  <Button variant="outline-dark" type="button" onClick={this.clearRegisterForm}>
                    Törlés
                  </Button>
                  <Button variant="primary" type="submit" className="m-2">
                    Tovább
                  </Button>
                </div>
              </Row>
            </Form>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default AdminAddUser;
