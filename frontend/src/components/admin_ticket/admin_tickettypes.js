import React from 'react';
import './admin_tickettypes.scss';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Trash } from 'react-bootstrap-icons';
import { PencilSquare } from 'react-bootstrap-icons';
import { PlusSquare } from 'react-bootstrap-icons';


class AdminTicketTypes extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
          tickettypes: [],
          tickettypealign: [],
        }
        this.ticketTypeFormChange = this.ticketTypeFormChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount()
    {
        this.ticketTypeNameInput.focus();

        // Get all ticketype from the server
        const request = new Request(`${process.env.REACT_APP_BACKEND_SERVER}/get_tickettypes`);
        fetch(request).then(response =>
        {
            const status = response.status;
            if (status === 200)
            {
                response.json().then(data =>
                {
                    this.setState(                    
                      {
                        tickettypes: data.tickettypes,
                        tickettypealign: ["right", "right", "right", "right", "right", "right", "right", "right", "right", "right", "right"],
                      }
                    );
                })
            }
            else
            {
                response.json().then(serverError =>
                {
                    alert(response.status + '\n' + serverError.message);
                });
            }
        });
    }

    ticketTypeFormChange(event)
    {
        this.setState(
        {
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event)
    {
        event.preventDefault();

        const ticketTypeData =
        {
            name: this.state.ticketTypeName,
        };

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        const options =
        {
            method: 'POST',
            headers,
            body: JSON.stringify(ticketTypeData)
        };

        const request = new Request(`${process.env.REACT_APP_BACKEND_SERVER}/admin_addtickettype`, options);

        fetch(request).then(response =>
        {
            const status = response.status;
            if (status === 201)
            {
              const tickettypes = this.state.tickettypes.concat({name:this.state.ticketTypeName});
              this.setState({ tickettypes });
              this.ticketTypeForm.reset();
              //alert('Sikeresen felvettél egy tikettípust!');
                
            }
            else
            {
                response.json()
                    .then(serverError =>
                    {
                        alert(response.status + '\n' + serverError.message);
                    });
            }
        });
    }

    OnDragPrint(param)
    {
      console.log(param._reactName + " " + param.target.innerHTML + " " + param.target.id);
    }

    OnDragEnter = (param) =>
    {
      console.log(param._reactName + " " + param.target.innerHTML + " " + param.target.id);
      this.setState(prevState => {
        /*const list = prevState.tickettypealign.map( (item, idx) => 
        {
          item = (idx===parseInt(param.target.id)) ? "Left" : "right";
          console.log("Szerinted egyenlők? " + idx + " " + param.target.id + " " + item);
        });
        return {list,};*/
        prevState.tickettypealign[param.target.id] = "left";
        const list = prevState;
        return {list,};
      })
      console.log(this.state.tickettypealign);
    }

    OnDragLeave = (param) =>
    {
      console.log(param._reactName + " " + param.target.innerHTML + " " + param.target.id);
      this.setState(prevState => {
          /*const list = prevState.tickettypealign.map( (item) => item = "right");

          return {list,};*/
          prevState.tickettypealign[param.target.id] = "right";
          const list = prevState;
          return {list,};
      })
      console.log(this.state.tickettypealign);
    }

  render() {
    const tableRow = this.state.tickettypes.map((tickettype, index) =>
      <tr key={index + 1}>
        <td align="middle" valign="top">{index + 1}</td>
        <td id={index + 1} align={this.state.tickettypealign[index+1]} onDragStart={this.OnDragPrint} onDragEnd={this.OnDragPrint} onDragLeave={this.OnDragLeave} onDragEnter={this.OnDragEnter} >{tickettype.name}</td>
        <td align="middle">
          <Button variant="outline-danger" name={`${tickettype._id}:${index}`} type="button" size="sm" onClick={this.deleteTicketType}>
            Törlés <Trash className="mx-1"/>
          </Button>
        </td>
        <td align="middle">
          <Button variant="outline-info" name={`${tickettype._id}:${tickettype.name}`} type="button" size="sm" onClick={this.updateTicketType}>
           Szerkesztés <PencilSquare className="mx-1"/>
          </Button>
        </td>
      </tr>
    );

    return (
    <div align="center">
    <div className="AdminTicketTypes"><br/><br/><br/><br/>
    <Card style={{width: '90%'}} className="p-4 bg-light text-center">
      <Card.Body className="card-body"> 
        <h3 align="left">Jelenlegi típusok</h3>
        <h6 align="left">Ezek a típusok vannak jelenleg az adatbázisban</h6>
        <hr/><br/>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Típusnév</th>
              <th>Törlés</th>
              <th>Szerkesztés</th>
            </tr>
          </thead>
          <tbody>
            {tableRow}
          </tbody>
        </Table>
        <br/>
        <Form ref={(form) => {this.ticketTypeForm = form;}} onSubmit={this.handleSubmit}>
          <Form.Group as={Row} controlId="ticketTypeName">
            <Form.Label column="column" sm="2">Új felvétele</Form.Label>
            <Col sm="8">
              <Form.Control ref={(ticketType) => {this.ticketTypeNameInput = ticketType;}} name="ticketTypeName" type="text" required onChange={this.ticketTypeFormChange}/>
            </Col>
            <Col sm="2">
            <Button variant="primary" type="submit" value="Submit">
            Hozzáadás <PlusSquare className="mx-1"/>
            </Button>
            </Col>
          </Form.Group>
        </Form>
      </Card.Body>
    </Card>
  </div>
  <br/>
  </div>
  )
};
}

export default AdminTicketTypes;
