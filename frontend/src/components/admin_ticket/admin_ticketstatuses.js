import React from 'react';
import './admin_ticketstatuses.scss';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Trash } from 'react-bootstrap-icons';
import { PencilSquare } from 'react-bootstrap-icons';
import { PlusSquare } from 'react-bootstrap-icons';


class AdminTicketStatuses extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
          ticketstatuses: [],
        }
        this.ticketStatusFormChange = this.ticketStatusFormChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount()
    {
        this.ticketStatusNameInput.focus();

         // Get all ticketstatuses from the server
         const request = new Request(`${process.env.REACT_APP_BACKEND_SERVER}/get_ticketstatuses`);
         fetch(request).then(response =>
         {
             const status = response.status;
             if (status === 200)
             {
                 response.json().then(data =>
                 {
                     this.setState(
                     {
                         ticketstatuses: data.ticketstatuses
                     })
                 })
             }
             else
             {
                 response.json().then(serverError =>
                 {
                     alert(response.status + '\n' + serverError.message);
                 });
             }
         });
    }

    ticketStatusFormChange(event)
    { 
      this.setState(
        {
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event)
    {
        event.preventDefault();

        console.log(this.status);

        const ticketStatusData =
        {
            name: this.state.ticketStatusName,
        };

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        const options =
        {
            method: 'POST',
            headers,
            body: JSON.stringify(ticketStatusData)
        };

        const request = new Request(`${process.env.REACT_APP_BACKEND_SERVER}/admin_addticketstatus`, options);

        fetch(request).then(response =>
        {
            const status = response.status;
            if (status === 201)
            {
                const ticketstatuses = this.state.ticketstatuses.concat({name:this.state.ticketStatusName});
                this.setState({ ticketstatuses });
                this.ticketStatusForm.reset();
                //alert('Sikeresen felvettél egy tiketállapotot!');
            }
            else
            {
                response.json()
                    .then(serverError =>
                    {
                        alert(response.status + '\n' + serverError.message);
                    });
            }
        });
    }

  render() {
    const tableRow = this.state.ticketstatuses.map((ticketstatus, index) =>
    <tr key={index + 1}>
      <td align="middle" valign="top">{index + 1}</td>
      <td align="right">{ticketstatus.name}</td>
      <td align="middle">
        <Button variant="outline-danger" name={`${ticketstatus._id}:${index}`} type="button" size="sm" onClick={this.deleteTicketStatus}>
          Törlés <Trash className="mx-1"/>
        </Button>
      </td>
      <td align="middle">
        <Button variant="outline-info" name={`${ticketstatus._id}:${ticketstatus.name}`} type="button" size="sm" onClick={this.updateTicketStatus}>
         Szerkesztés <PencilSquare className="mx-1"/>
        </Button>
      </td>
    </tr>);


    return (
    <div align="center">
    <div className="AdminTicketStatuses"><br/><br/>
    <Card style={{width: '90%'}} className="p-4 bg-light text-center">
      <Card.Body className="card-body">
        <h3 align="left">Jelenlegi állapotok</h3>
        <h6 align="left">Ezek az állapotok vannak jelenleg az adatbázisban</h6>
        <hr/><br/>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Állapotnév</th>
              <th>Törlés</th>
              <th>Szerkesztés</th>
            </tr>
          </thead>
          <tbody>
            {tableRow}
          </tbody>
        </Table>
        <br/>
        <Form ref={(form) => {this.ticketStatusForm = form;}} onSubmit={this.handleSubmit}>
            <Form.Group as={Row} controlId="ticketStatusName">
              <Form.Label column="column" sm="2">Új felvétele</Form.Label>
              <Col sm="8">
                <Form.Control ref={(ticketStatus) => {this.ticketStatusNameInput = ticketStatus;}} name="ticketStatusName" type="text" required onChange={this.ticketStatusFormChange}/>
              </Col>
              <Col sm="2">
              <Button variant="primary" type="submit" value="Submit">
              Hozzáadás <PlusSquare className="mx-1"/>
              </Button>
            </Col>
            </Form.Group>
          </Form>
      </Card.Body>
    </Card>
    <br/><br/>
  </div>
  <br/>
  </div>
  )
};
}
export default AdminTicketStatuses;