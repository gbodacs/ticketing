import React from 'react';
import './header.scss';
import { LinkContainer } from 'react-router-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Nav from 'react-bootstrap/Nav';
import Cookies from 'universal-cookie';

const cookies = new Cookies();
class Header extends React.Component
{
    constructor(props)
    {
        super(props);
        this.logoutUser = this.logoutUser.bind(this);
    }

    logoutUser()
    {
        sessionStorage.removeItem('userId');
        sessionStorage.removeItem('userName');
        sessionStorage.removeItem('isAdmin');

        const request = new Request(`${process.env.REACT_APP_BACKEND_SERVER}/logout`);

        fetch(request).then(response =>
        {
            const status = response.status;
            if (status === 200)
            {
                cookies.remove('login',
                {
                    path: '/'
                });
                this.props.history.push('/login');
            }
        });
    }

    render() 
    {
        const isAdmin = this.props.isAdmin;
        let header;

        if (isAdmin === 'true') 
        {
            header = (
                <Nav className="me-auto">
                <NavDropdown className="my-auto" title="Felhasználók" id="nav-dropdown">
                    <LinkContainer to="/home/admin_adduser">
                    <NavDropdown.Item>Hozzáadás</NavDropdown.Item>
                    </LinkContainer>
                    <NavDropdown.Divider />
                    <LinkContainer to="/home/admin_getusers">
                    <NavDropdown.Item>Megtekintés</NavDropdown.Item>
                    </LinkContainer>
                </NavDropdown>
                <LinkContainer to="/home/admin_tickettypes">
                    <Nav.Link className="my-auto">Tiket típusok</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/home/admin_ticketstatuses">
                    <Nav.Link className="my-auto">Tiket állapotok</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/home/admin_tickets">
                    <Nav.Link className="my-auto">Ticketek</Nav.Link>
                </LinkContainer>
                </Nav>
            );
        } else 
            {
            header = (
                <Nav className="me-auto">
                <LinkContainer to="/home/user_tickets">
                    <Nav.Link className="my-auto">Ticketek</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/home/user_filters">
                    <Nav.Link className="my-auto">Szűrők</Nav.Link>
                </LinkContainer>
                </Nav>
            );
            }
            return (
            <div className="Header">
                <Navbar collapseOnSelect bg="primary" variant="light" expand="md" fixed="top">
                    <Navbar.Brand href="#home"><img alt="" src="/image/logo30x30.png" width="30" height="30" className="d-inline-block align-top"/>{' '}Your Brand</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <Navbar.Collapse id="basic-navbar-nav">
                        {header}
                        <Nav>
                            <b><Nav.Link onClick={this.logoutUser}>Kilépés</Nav.Link></b>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        );
    }
}

export default Header;
