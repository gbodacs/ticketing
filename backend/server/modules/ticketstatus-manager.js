const moment = require('moment');
const TicketStatus = require('../models/ticketstatus');

/**********************************************
	TicketStatus insertion, update & deletion methods
 **********************************************/

    exports.addNewTicketStatus = function(newData, callback)
    {
        TicketStatus.findOne(
        {
            name: newData.statusname
        }, function(error, status)
        {
            if (status)
                callback('status-already-exist');
            else
            {
                // append date stamp when record was created //
                const status = new TicketStatus(newData);
                status.save()
                    .then(newStatus =>
                    {
                        callback(null);
                    })
                    .catch(error =>
                    {
                        callback(error);
                    });
            }
        });
    };
    
    exports.updateStatus = function(newData, callback)
    {
     /*   var o = {
            statusname: newData.statusname,
        };
    
        Status.findOneAndUpdate(
        {
            _id: getObjectId(newData.id)
        },
        {
            $set: o
        },
        {
            returnOriginal: false
        }, callback);*/
    };
    
    exports.getAllTicketStatus = function(callback)
    {
        TicketStatus.find(
        {}, (error, statuses) =>
        {
            if (error)
                callback(error);
            else
                callback(null, statuses);
        });
    };
    
    exports.deleteStatus = function(statusname, callback)
    {
        TicketStatus.deleteOne(
        {
            name: newData.statusname
        }, callback);
    };
    