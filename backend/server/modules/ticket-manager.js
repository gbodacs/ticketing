
    /**********************************************
        Block insertion, update & deletion methods
     **********************************************/
    
        exports.addNewBlock = function(newData, callback)
        {
            Block.findOne(
            {
                name: newData.name
            }, (error, block) =>
            {
                if (block)
                {
                    callback('block-name-taken');
                }
                else
                {
                    // append date stamp when record was created //
                    newData.date = moment().format('MMMM Do YYYY, h:mm:ss a');
                    const newBlock = new Block(newData);
                    newBlock.save()
                        .then(newBlock =>
                        {
                            callback(null);
                        })
                        .catch(error =>
                        {
                            callback(error);
                        });
                }
            });
        };
        
        exports.updateblock = function(newData, callback)
        {
            var o =
            {
                name: newData.name,
                unit: newData.unit,
                excercises: newData.excercises,
                done: newData.done
            };
        
            blockdb.findOneAndUpdate(
            {
                _id: getObjectId(newData.id)
            },
            {
                $set: o
            },
            {
                returnOriginal: false
            }, callback);
        };
        
        exports.deleteBlock = function(id, callback)
        {
            blockdb.deleteOne(
            {
                _id: getObjectId(id)
            }, callback);
        };
        
        exports.getAllBlocks = function(callback)
        {
            Block.find({}, (error, blocks) =>
            {
                if (error)
                    callback(error);
                else
                    callback(null, blocks);
            });
        };
        
        /**********************************************
            DailyPlan insertion, update & deletion methods
         **********************************************/
        
        exports.addNewDailyPlan = function (newData, callback) 
        {
          Dailyplan.findOne({
            $and: [{
                $or: [{
                    $and: [{
                      startDate: {
                        $lte: new Date(newData.startDate)
                      }
                    }, {
                      endDate: {
                        $gte: new Date(newData.startDate)
                      }
                    }]
                  },
                  {
                    $and: [{
                      startDate: {
                        $lte: new Date(newData.endDate)
                      }
                    }, {
                      endDate: {
                        $gte: new Date(newData.endDate)
                      }
                    }]
                  },
                  {
                    $and: [{
                      startDate: {
                        $lte: new Date(newData.startDate)
                      }
                    }, {
                      endDate: {
                        $gte: new Date(newData.endDate)
                      }
                    }]
                  },
                  {
                    $and: [{
                      startDate: {
                        $gte: new Date(newData.startDate)
                      }
                    }, {
                      endDate: {
                        $lte: new Date(newData.endDate)
                      }
                    }]
                  }
                ]
              },
              {
                userId: newData.userId
              }
            ]
          }, (error, dailyplan) =>
            {
                if (dailyplan)
                {
                    callback('This user already have a dailyplan for the selected date!');
                }
                else if (error)
                {
                    callback(error.message.message);
                }
                else
                {
                    // append date stamp when record was created //
                    newData.date = moment().format('MMMM Do YYYY, h:mm:ss a');
                    const newDailyplan = new Dailyplan(newData);
                    newDailyplan.save().then(newDailyplan =>
                    {
                        callback(null);
                    })
                    .catch(error =>
                    {
                        callback(error);
                    });
                }
            });
        };
        
        exports.getUserAllDailyPlanDates = function(userData, callback)
        {
            Dailyplan.find(
            {
                userId: userData.userId
            }).select('startDate endDate -_id')
                .exec((error, dailyplanDates) =>
                {
                    let dates = [];
                    let oneDayinMs = 86400000;
                    if (error)
                    {
                        callback(error);
                    }
                    else
                    {
                        dailyplanDates.map(dailyplan =>
                        {
                            let days = Math.round((dailyplan.endDate - dailyplan.startDate) / oneDayinMs);
                            let startDate = Math.round(new Date(dailyplan.startDate).getTime() / 1000);
                            let nextDate = new Date(dailyplan.startDate).getTime();
                            dates.push(startDate)
                            for (let i = 1; i < days; i++)
                            {
                                nextDate += oneDayinMs;
                                dates.push(nextDate / 1000);
                            }
                        });
                        callback(null, dates);
                    }
                });
        };
        
        /*exports.updateDailyPlan = function(newData, callback)
        {
            var o =
            {
                name : newData.name,
                unit : newData.unit,
                excercises: newData.excercises,
                done: newData.done
            };
        
            blockdb.findOneAndUpdate({_id:getObjectId(newData.id)}, {$set:o}, {returnOriginal : false}, callback);
        };
        
        exports.deleteBlock = function(id, callback)
        {
            blockdb.deleteOne({_id:getObjectId(id)}, callback);
        };*/
        
        exports.getUserDailyPlan = function(userData, callback)
        {
            Dailyplan.aggregate([
            {
                $match:
                {
                    startDate:
                    {
                        $lte: new Date(userData.date)
                    },
                    endDate:
                    {
                        $gte: new Date(userData.date)
                    },
                    userId: userData.id
                }
            },
            {
                $lookup:
                {
                    "from": "blocks",
                    "localField": "id",
                    "foreignField": "id",
                    "as": "allBlocks"
                }
            },
            {
                $lookup:
                {
                    "from": "exercises",
                    "localField": "exerciseList",
                    "foreignField": "id",
                    "as": "allExercises"
                }
            }]).exec(function(error, dailyplan)
            {
                if (error)
                {
                    callback(error);
                }
                else
                {
                    callback(null, dailyplan);
                }
            });
        };