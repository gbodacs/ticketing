const moment = require('moment');
const TicketType = require('../models/tickettype');

/**********************************************
	Ticket-Type insertion, update & deletion methods
 **********************************************/

    exports.addNewTicketType = function(newData, callback)
    {
        TicketType.findOne(
        {
            name: newData.typename
        }, function(error, type)
        {
            if (type)
                callback('type-already-exist');
            else
            {
                // append date stamp when record was created //
                const type2 = new TicketType(newData);
                type2.save()
                    .then(newType =>
                    {
                        callback(null);
                    })
                    .catch(error =>
                    {
                        callback(error);
                    });
            }
        });
    };
    
    exports.updateTicketType = function(newData, callback)
    {
     /*   var o = {
            typename: newData.typename,
        };
    
        Type.findOneAndUpdate(
        {
            _id: getObjectId(newData.id)
        },
        {
            $set: o
        },
        {
            returnOriginal: false
        }, callback);*/
    };
    
    exports.getAllTicketType = function(callback)
    {
        TicketType.find(
        {}, (error, types) =>
        {
            if (error)
                callback(error);
            else
                callback(null, types);
        });
    };
    
    exports.deleteTicketType = function(typename, callback)
    {
        TicketType.deleteOne(
        {
            typename: newData.typename
        }, callback);
    };
    