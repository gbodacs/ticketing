const mongoose = require('mongoose');

const ticketStatusSchema = mongoose.Schema(
{
  name: { type: String, required: true}
});

module.exports = mongoose.model('TicketStatus', ticketStatusSchema);
