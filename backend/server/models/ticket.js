const mongoose = require('mongoose');

const ticketSchema = mongoose.Schema(
{
  name: { type: String, required: true},
  summary: { type: String, required: true},
  status: { type: String, required: true},
  type: { type: String, required: true},
  labels: [{id: String}],
  description: { type: String, required: false},
  assignee: { type: String, required: false},
  createdby: { type: String, required: true},
  product: { type: String, required: false},
  component: { type: String, required: false},
  createdate: { type: Date, required: true}
});

module.exports = mongoose.model('Ticket', ticketSchema);
