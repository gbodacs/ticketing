const mongoose = require('mongoose');

const ticketTypeSchema = mongoose.Schema(
{
  name: { type: String, required: true}
});

module.exports = mongoose.model('TicketType', ticketTypeSchema);
